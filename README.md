# Action Item: Static Deployment 📥

In this `Action Item` you will:

- ☁️ Get an `AWS Account` and learn the basics
- 🏗️ Deploy a Front End application using `AWS S3`
- 📖 Use `GitLab` to set up `an automatic release pipeline`

## Challenges:

1. [x] 💪[COMPETENT] Get an account on `GitLab`
2. [ ] 💪[COMPETENT] Get an `AWS` account
3. [ ] 💪[COMPETENT] Create a `GitLab` repository for the app
4. [ ] 💪[COMPETENT] Change the origin of our repository to the `Gitlab`
5. [ ] 💪[COMPETENT] Create an `S3` `bucket` and `deploy` the app manually
6. [ ] 🏋🏽‍♀️[PROFICIENT] Automatize the deployment by creating a pipeline in `Gitlab`
7. [ ] 🏋🏽‍♀️[PROFICIENT] Add a `staging` bucket and a new stage to the pipeline

> Make sure you set aside at least 2 hours of focused, uninterrupted work and give your best.

---

### <a name="app-structure"></a> App Architecture

![app-structure](examples/app_structure/app_structure.png)

## App Setup:

Install dependencies:

```bash
npm install
```

Run in `production` mode:

```bash
npm run prod
```

> NOTE: deploying to static storage(like S3) mean we have no server - we are just storing our files and serving them over the network. That means we cannot use features like Server Side Rendering or Server Side Routing that need a server side component.

---

<details closed>
<summary>CLICK ME! - 1. SOLUTION: Get an account on GitLab</summary>

### 1. Get an account on `GitLab`

Go to [Gitlab](https://gitlab.com/users/sign_up) and get an account. We recommend using your `Github` when signing up.
![Gitlab-Sing-up](examples/gitlab_sign_up.png)

1.2 Add your credit card to validate your account(you will not be able to run pipelines if you do not do this).

See [why Gitlab asks for this here](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/).

1.3 Add your `.ssh` key to the `Gitlab` account. See the [step-by-step guide here](https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account).

**The two steps above are critical for you to be able to push code and run pipelines on Gitlab!**

> _If you already have a Gitlab account and the `ssh key` setup you can skip the two steps above._

</details>

---

<details closed>
<summary>CLICK ME! - 2. SOLUTION: Get an AWS Account</summary>

### 2. Create an account on `AWS`

Go to [AWS](https://portal.aws.amazon.com/billing/signup#/start) and create an account.

</details>

---

<details closed>
<summary>CLICK ME! - 3. SOLUTION: Create a GitLab repository for the app</summary>

### 3. Create a `GitLab` repository for the app

3.1 Login to `Gitlab` by clicking [here](https://gitlab.com/users/sign_in)

3.2 Go to [Projects] and click on _New Project_:
![New Project](examples/new_project_gitlab.png)

3.3 In the next step select _Blank Project_:
![Blank Project](examples/blank_project_gitlab.png)

3.4 Add a name and remove the _README_ option:
![Setup the Project](examples/new_project_gitlab_setup.png)

3.5 Click _Create Project_, you should see this:

![New Project](examples/new_project_view_gitlab.png)

</details>

---

<details closed>
<summary>CLICK ME! - 4. SOLUTION: Change the origin of our repository to the Gitlab</summary>

### 4. Change the origin of our repository to the Gitlab repository

From the project page you have created above, scroll to the bottom and copy the code at the bottom of the page, leaving out the first line:

![New Project](examples/push_an_existing_folder.png)

Paste that in a terminal window, inside your repo:

```bash
git remote rename origin old-origin
git remote add origin git@gitlab.com:YOUR_USERNAME/YOUR_REPOSITORY
git push -u origin --all
git push -u origin --tags
```

You should see something like this:

![New Project](examples/push_sucess.png)

**Note: If you get errors here, make sure you review step #1**

Your code should be now visible in the `Repository` tab:

![Repo Setup](examples/repository_setup.png)

</details>

---

<details closed>
<summary>CLICK ME! - 5. SOLUTION: Create an S3 bucket and deploy the app manually</summary>

### 5. Create an `S3 bucket` and `deploy` the app manually

Before we automate the deployment, we will make a test and `deploy manually`.

> NOTE: Always automate AFTER you tested everything in the local environment or manually!

#### 5.1 Run the app locally

```bash
npm run prod
```

Make sure the app works. Check everything runs in the browser:

![New Project](examples/task_5/app_browser.png)

Then run the `tests`:

```bash
npm test
```

#### 5.2 Run the `build` locally:

```bash
npm run build
```

#### 5.3 Create an `S3` bucket on `AWS`:

![Navigate to S3](examples/navigate_to_s3.png)

---

![Click Create Bucket](examples/click_create_bucket.png)

---

![Add Bucket Name](examples/add_bucket_name.png)

---

![Enable Public Access](examples/bucket-public-access.png)

---

![Click Create Bucket](examples/create_bucket_action.png)

#### 5.4 Enable `Static Website Hosting` for the bucket you have created.

![Select Create Bucket](examples/select_s3_bucket.png)

---

![Bucket Properties](examples/bucket_static_website.png)

---

![Bucket Properties](examples/static_website_two_click.png)

---

![Bucket Properties](examples/static_website_three_enable.png)

**Check the [step-by-step guide to enable website hosting on an AWS Bucket here.](https://docs.aws.amazon.com/AmazonS3/latest/userguide/EnableWebsiteHosting.html`)**

---

#### 5.4 Upload the content of the build folder:

![Upload Step 1](examples/upload_step_one.png)

---

![Upload Step 2](examples/upload_step_two.png)

---

![Upload Step 3](examples/upload_step_three.png)

---

![Upload Step 4](examples/upload_step_four.png)

---

![Upload Step 5](examples/upload_step_five.png)

---

![Upload Step 6](examples/upload_step_six.png)

---

![Upload Step 7](examples/task_5/app_live.png)

> NOTE: Congratulations!🎉🎉🎉 Now its time to automate the deployment using `Gitlab`.

</details>

---

<details closed>
<summary>CLICK ME! - 6. SOLUTION: Automatize the deployment by creating a pipeline in Gitlab</summary>

### 6. Automatize the `deployment` by creating a pipeline in `Gitlab`

Now it is time to add `automation`, so we can focus on what matters.

#### 1. In `Gitlab` go to `Pipelines`

![Create Pipeline Step 1](examples/add_pipeline_step_one.png)

#### 2. Click on **"Create New Pipeline"** && remove the default code

![Create Pipeline Step 2](examples/add_pipeline_step_two.png)

#### 3. Add the following code instead:

```yaml
image: node:lts
stages: ["build", "test", "deploy"]

# WARNING
# This pipeline needs the following variables set up to work:
# AWS_ACCESS_KEY_ID =  // the key id to connect to AWS
# AWS_SECRET_ACCESS_KEY = // the secret key to connect to AWS
# AWS_DEFAULT_REGION = // the default region in AWS
# AWS_BUCKET_NAME = // the name of the bucket

# The cached folders between different pipeline jobs
cache:
  paths:
    - node_modules/

# The Build Pipeline Section
lint:
  stage: build
  script:
    - npm install
    - npm run lint

build:
  stage: build
  script:
    - npm run build
  artifacts:
    paths:
      - build
    expire_in: 1 week

#running unit tests
test:
  stage: test
  script:
    - npm run test
  artifacts:
    paths:
      - coverage
    expire_in: 1 week

# Deployment Section

# the deploy job
deploy-job: # This job runs in the deploy stage.
  stage: deploy # It only runs when *both* jobs in the test stage complete successfully.
  image: python:latest
  script:
    - pip install awscli
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set region $AWS_DEFAULT_REGION
    - aws s3 cp build s3://$AWS_BUCKET_NAME/ --recursive --acl public-read
```

#### 5. Save the pipeline:

![Create Pipeline Step 3](examples/add_pipeline_step_three.png)

#### 6. Check the running pipeline:

![Create Pipeline Step 5](examples/add_pipeline_step_four.png)

#### 7. Notice the `pipeline` and the `pipeline stages`:

![Create Pipeline Step 6](examples/add_pipeline_step_five.png)

#### 8. Check the individual `task` and the `artifact produces`:

![Create Pipeline Step 6](examples/add_pipeline_step_six.png)

#### 9. The `pipeline` should fail as the `environment` variables are missing:

![Create Pipeline Step 7](examples/add_pipeline_step_seven.png)

#### 10. Go to `Project Settings` and click on `CI/CD`:

![Create Pipeline Step 8](examples/add_pipeline_step_eight.png)

#### 11. Click `Add variable`:

![Create Pipeline Step 9](examples/add_pipeline_step_nine.png)

#### 11. Add your first variable:

![Create Pipeline Step 10](examples/add_pipeline_step_ten.png)

_\*\*For the other variables we will need to create a `CLI` user in AWS. Gitlab will use that to run the `AWS CLI` and upload the build folder programmatically._

#### 12. Go to [AWS users in `IAM`](https://console.aws.amazon.com/iamv2/home?#/users)

#### 13. Click `Add users`:

![Create Pipeline Step 11](examples/add_pipeline_step_eleven.png)

#### 14. Add a username and check the right user type:

![Create Pipeline Step 12](examples/add_pipeline_step_twelve.png)

#### 15. In the next step attach the `S3 Full Access` policy to the user:

![Create Pipeline Step 13](examples/add_pipeline_step_thirteen.png)

#### 17. Add the two credentials to `Gitlab` variables:

![Create Pipeline Step 14](examples/add_pipeline_step_fourteen.png)

#### 18. Add your default `AWS` region as a `Gitlab` variable:

![Create Pipeline Step 15](examples/add_pipeline_step_fifteen.png)

#### 19. Run the `pipeline` again manually:

![Create Pipeline Step 16](examples/add_pipeline_step_sixteen.png)

#### 20. Notice the upload to `S3` is done programmatically:

![Create Pipeline Step 17](examples/add_pipeline_step_seventeen.png)

#### 21. Your `pipeline` should now pass:

![Create Pipeline Step 18](examples/add_pipeline_step_eighteen.png)

**Congratulations!! You have now a fully functional pipeline on Gitlab and can release code quickly and safely.**

**For reference, you can check [our Gitlab project here.](https://gitlab.com/codewithdragos/ci-cd-frontend)**

</details>

---

<details closed>
<summary>CLICK ME! - 7. SOLUTION: Add a staging bucket and a new stage to the pipeline</summary>

### 7. Add a `staging bucket` and a new stage to the pipeline

It would also be great if we could add a `rollback` mechanism or a `staging` environment.

1. Change code and push to `main`, observe the running pipeline
2. Add a new `staging` bucket and a new `staging` deployment that only runs on the `staging` branch
3. Restrict the deployment to the production bucket to run only when a commit is made to the `main` branch
</details>

---

### Getting Help

If you have issues with the Action Item, you can ask for help in the [Community](https://community.theseniordev.com/) or in the [Weekly Q&A’s](https://calendar.google.com/calendar/u/0?cid=Y19kbGVoajU1Z2prNXZmYmdoYmxtdDRvN3JyNEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t).

### Made with :orange_heart: in Berlin by @TheSeniorDev
